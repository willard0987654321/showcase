## Создание ВМ на яндексе через терраформ и cli яндекса (выжимка из документации яндекса, официальной документации терраформа и "практической работы")
***
1. Установка терраформа:
- Зеркало терраформа ( https://hashicorp-releases.yandexcloud.net/terraform/ )
- Установка переменной path 
```
export PATH=$PATH:/path/to/terraform
```
***
2. Установка cli 
- скачивание скрипта на установку:
```
curl -sSL https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash
```
- получение токена (естественно, уже с акком яндекс облака, и платежным акком)
```
https://oauth.yandex.com/authorize?response_type=token&client_id=1a6990aa636648e9b2ef855fa7bec2fb
```
- через yc init завершить установку. Потребует токена, выбор облака, если их несколько к акку привязано, дефолтную папку для работы и "зону доступности".
***
3. Сервисный аккаунт
- Создание: 
```
yc iam service-account create --name <name>
```
- Назначение ролей:
```
yc <service-name> <resource> add-access-binding <resource-name>|<resource-id> \
  --role <role-id> \
  --subject serviceAccount:<service-account-id> 
```
где:
<service-name> — название сервиса, на чей ресурс назначается роль, например resource-manager. 
<resource> — категория ресурса, например cloud. (в этом случае folder)
<resource-name> — имя ресурса. Вы можете указать ресурс по имени или идентификатору.
<resource-id> — идентификатор ресурса. (b1g58ddh730ae7u017t8)
<role-id> — назначаемая роль, например resource-manager.clouds.owner. (в случае работы с вм - compute.admin)
<service-account-id> — идентификатор сервисного аккаунта, которому назначается роль. (ajepjq5e25edc2emrfgp)
```
yc resource-manager folder add-access-binding b1g58ddh730ae7u017t8 --role compute.admin --subject serviceAccount:ajepjq5e25edc2emrfgp
```
***
4. Настройка профиля CLI для работы через сервисный акк.
- Создание ключа
```
yc iam key create \
  --service-account-id <идентификатор_сервисного_аккаунта> \
  --folder-name <имя_каталога_с_сервисным_аккаунтом> \
  --output key.json
```
Где:
<service-account-id> — идентификатор сервисного аккаунта. (ajepjq5e25edc2emrfgp)
<folder-name> — имя каталога, в котором создан сервисный аккаунт. (yandex-cloud)
<output> — имя файла с авторизованным ключом. (whatever.json)
```
yc iam key create --service-account-id ajepjq5e25edc2emrfgp --folder-name yandex-cloud --output vm-admin-key.json
```

- Создание профиля:
```
yc config profile create vm-admin
```
- Задание конфигурации:
```
yc config set service-account-key key.json
yc config set cloud-id <идентификатор_облака>
yc config set folder-id <идентификатор_каталога>  
```
где:
<service-account-key> — файл с авторизованным ключом сервисного аккаунта.
<cloud-id> — идентификатор облака.
<folder-id> — идентификатор каталога.
```
yc config set service-account-key vm-admin-key.json
yc config set cloud-id b1ge12tcvfm39a8vsvpt
yc config set folder-id b1g58ddh730ae7u017t8
```

- Добавление переменных окружения:
```
export YC_TOKEN=$(yc iam create-token)
export YC_CLOUD_ID=$(yc config get cloud-id)
export YC_FOLDER_ID=$(yc config get folder-id)
```
где:
<YC_TOKEN> — IAM-токен.
<YC_CLOUD_ID> — идентификатор облака.
<YC_FOLDER_ID> — идентификатор каталога.
***
5. Настройка провайдера терраформа
- Сам файл конфигурации cli терраформа (пихать в хомяка)
```
nano ~/.terraformrc
```
```
provider_installation {
  network_mirror {
    url = "https://terraform-mirror.yandexcloud.net/"
    include = ["registry.terraform.io/*/*"]
  }
  direct {
    exclude = ["registry.terraform.io/*/*"]
  }
}
```
(в винде это terraform.rc, находится в appdata)
***
6. Создание ВМ.
- обязательный блок настроек провайдера 
```
terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  zone = "<зона доступности по умолчанию>"
}
```
где:
<source> — глобальный адрес источника провайдера.
<required_version> — минимальная версия Terraform, с которой совместим провайдер.
<provider> — название провайдера.
<zone> — зона доступности, в которой по умолчанию будут создаваться все облачные ресурсы.
и где zone:
<ru-central1-a>
<ru-central1-b>
<ru-central1-c> (с ограничениями) 

- terraform init в папке с файлом конфигом для подтягивания провайдера.

- yc compute image list --folder-id standard-images - получение доступных образов из "хаба" яндекса. (there is a lot)

debian11 - f2eng95d29nh342t62po

### Итоговый вариант, прошедший terraform plan
```
terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  service_account_key_file = file("/home/rezolut/yandex-cloud/vm-admin-key.json")
  zone = "ru-central1-a"
  

}

resource "yandex_compute_instance" "vm-1" {
  name = "gate"

  resources {
    cores  = 1
    memory = 1
    core_fraction = 5
  }

  boot_disk {
    initialize_params {
      image_id = "f2eng95d29nh342t62po"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  metadata = {
    user-data = "/home/rezolut/MainRepo/terraform/users"
  }
}
resource "yandex_compute_instance" "vm-2" {
  name = "rezolut-fun-box"

  resources {
    cores  = 2
    memory = 4
    core_fraction = 5
  }

  boot_disk {
    initialize_params {
      image_id = "f2eng95d29nh342t62po"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  metadata = {
    user-data = "/home/rezolut/MainRepo/terraform/users"
  }
}

resource "yandex_vpc_network" "network-1" {
  name = "network1"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "main-subnet"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["172.16.0.0/24"]
}

output "internal_ip_address_vm_1" {
  value = yandex_compute_instance.vm-1.network_interface.0.ip_address
}

output "internal_ip_address_vm_2" {
  value = yandex_compute_instance.vm-2.network_interface.0.ip_address
}


output "external_ip_address_vm_1" {
  value = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
}
```

## нюансы:

- core_fraction = 5 - насколько знаю, яндекс без этого параметра создает машины с 100% использованием проца (хз как это в двух словах нахвать). Поскольку есть варианты в вэб морде 20%, 50% и 100%, логично что тут аргументы будут 5, 2, 1.

- авторизация сервисного аккаунта по ключу:
```
provider "yandex" {
  service_account_key_file = file("/home/rezolut/yandex-cloud/vm-admin-key.json")
  zone = "ru-central1-a"

-В какой-то момент с "радаров" пропал сервисный юзер, из-за чего запуск конфига не возможен, через ключ, логично...
  

}
```
В докумекнтации по поднятию этого нет, но провайдер требует token или ключ для авторизации. Учитывая что токен живет 12 часов, лучше использовать ключ. Плюс хранить такого типа токен в открытую не звучит как хорошая идея.

***
7. Создание юзеров.
Файл с параметрами юзеров. Должен быть в utf-8. Метадату в конфиге вмок надо менять на путь к этому файлу.
> В файле main.tf вместо ssh-keys задайте параметр user-data и укажите путь к файлу с метаданными:
```
users:
#cloud-config
  - name: sudo-chad
    groups: sudo
    shell: /bin/bash
    sudo: ['ALL=(ALL) NOPASSWD:ALL']
    ssh_authorized_keys:
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICN8rRxx/AUI3DUCTGY0QPv38ECxTYBxY4Jvy9Z9zXI3 rezolut@DESKTOP-EBR466E

  - name: rezolut
    groups: sudo
    shell: /bin/bash
    sudo: ['ALL=(ALL) NOPASSWD:ALL']
    ssh_authorized_keys:
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIF0h+vH/VaQQOLHD9XBrgY8bcZte2wBs34d9OUeDOMfG rezolut@DESKTOP-EBR466E
  
  - name: virgin-pleb
    shell: /bin/bash
    ssh_authorized_keys:
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDpKTUGDM1sP1dqxg/wTUrTXshFfr4X9ugzU6+Oba2P6 rezolut@DESKTOP-EBR466E
```

```
#cloud-config
```
большой нюнас, это не конфиг, без негоне распознается файл.

# Установка на новой машине
надо будет плейбук для этого сделать.

- скачать с зеркала яндекса бинарник терраформа
- прописать переменную
- скопировать terraformrc и переименовать его с добавлением точки впереди.
- скачать cli
- сделать новый ключ
- прописать его в файле main.tf
- прописать ssh ключи в машинах.