terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  cloud_id = "b1ge12tcvfm39a8vsvpt" 
  folder_id = "b1g58ddh730ae7u017t8"
  //service_account_key_file = "vm-admin-key.json"
  service_account_key_file = "vm-admin-key-workstation.json"
  zone = "ru-central1-a"


}

