resource "yandex_compute_instance" "vm-1" {
  name = "gate"

  resources {
    cores         = 2
    memory        = 1
    core_fraction = 5
  }

  boot_disk {
    initialize_params {
      image_id = "fd8i1mgcrhhshpjj0f95"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  

  metadata = {
    serial-port-enable = true
    user-data = "${file("users.txt")}"
    //ssh-keys = "ubuntu:${file("~/.ssh/yandex.pub")}"
  } 
}