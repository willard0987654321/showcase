resource "yandex_compute_instance" "vm-2" {
  name = "rezolut-fun-box"

  resources {
    cores         = 2
    memory        = 4
    core_fraction = 5
  }

  boot_disk {
    initialize_params {
      image_id = "fd8i1mgcrhhshpjj0f95"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
  }

  metadata = {
    serial-port-enable = true
    user-data = "${file("users.txt")}"
  }
}
